var dtInicio = FLUIGC.calendar('#dtInicio');
	$('#div_proc').hide();
		myTable = FLUIGC.datatable('#tabelData', {
   				dataRequest: [],
		 		search: {
        			enabled: false,
        		},
			    renderContent: ['titleEvento', 'status','dtEvento','hrEvento','localEvento', 'idEvento'],
			    header: [
			        { title: "Titulo" },
		            { title: "Status" },
		            { title: "Data do Evento" },
		            { title: "Hora do Evento"},
		            { title: "localEvento" },
					{ title:"ID" }
			    ],
				navButtons: {
                	enabled: true,
            	},
			}, function(err, data) {
			    // DO SOMETHING (error or success)
			});

		var contadorModal = 0;
			$('#busca').on('click', function(){
				$('#div_proc').show();
				$("#tabelData").children().eq(0).find("tr:gt(0)").remove();
				var ds = '';
				if($('#title').val() != null && $('#localEvento').val() != ''){
					var c1 = DatasetFactory.createConstraint('title',$('#title').val()[0],$('#title').val()[0],ConstraintType.MUST) 
					var c2 = DatasetFactory.createConstraint('localEvento',$('#localEvento').val(),$('#localEvento').val(),ConstraintType.MUST) 
					var c3 = DatasetFactory.createConstraint('metadata#active', true, true, ConstraintType.MUST)
					ds = DatasetFactory.getDataset('dsConvites', null, [c1,c2, c3], null).values;
					for(var i = 0; i < ds.length; i++){
						var newItem = {
							titleEvento: ds[i].title, 
							status: ds[i].valid,
							dtEvento: ds[i].dtEnvento,
							hrEvento: ds[i].hrEvento,
							localEvento: ds[i].localEvento,
							idEvento: ds[i].idEvento
						}

						myTable.addRow(0, newItem);
					}

				}else if($('#title').val() != null ){
					var c1 = DatasetFactory.createConstraint('title',$('#title').val()[0],$('#title').val()[0],ConstraintType.MUST)
					var c2 = DatasetFactory.createConstraint('metadata#active', true, true, ConstraintType.MUST)
					ds = DatasetFactory.getDataset('dsConvites', null, [c1, c2], null).values;
					for(var i = 0; i <= ds.length; i++){
						var newItem = {
							titleEvento: ds[i].title, 
							status: ds[i].valid,
							dtEvento: ds[i].dtEnvento,
							hrEvento: ds[i].hrEvento,
							localEvento: ds[i].localEvento,
							idEvento: ds[i].idEvento
						}
						console.log(newItem)
						myTable.addRow(0, newItem);
					}
				}else if($('#localEvento').val() != ''){
					var c1 = DatasetFactory.createConstraint('localEvento',$('#localEvento').val(),$('#localEvento').val(),ConstraintType.MUST) 
					var c2 = DatasetFactory.createConstraint('metadata#active', true, true, ConstraintType.MUST)
					ds = DatasetFactory.getDataset('dsConvites', null, [c1, c2], null).values;

					for(var i = 0; i < ds.length; i++){
						var newItem = {
							titleEvento: ds[i].title, 
							status: ds[i].valid,
							dtEvento: ds[i].dtEnvento,
							hrEvento: ds[i].hrEvento,
							localEvento: ds[i].localEvento,
							idEvento: ds[i].idEvento
						}

						myTable.addRow(0, newItem);
					}
				}

			});

			$("#tabelData").children().eq(0).children().on('dblclick', function(el,ev) {
				if(!$(this).children().is('th')) {
					myModal = FLUIGC.modal({
						title: 'Inscritos',
						content: '<div id="div_inscri" class="col-md-12" style="display:grid;clear:left;margin-top:15px;">'
									+'<div id="dataInscritos"></div>'
								+'</div>'
								+'<div id="extrai"></div>',
						id: 'fluig-modal',
						size: 'large',
						actions: [{
							'label':'Extrair dados da Pesquisa',
							'bind':'data-extrair-pesquisa'
						},
						{
							'label':'Enviar Pesquisa',
							'bind':'data-envia-email'
						},{
							'label':'Excel',
							'bind':'data-excel'
						},{
							'label': 'fechar',
							'bind': 'data-refresh-page',
							'autoClose': true
						}]
					}, function(err, data) {
						if(err) {
							// do error handling
						} else {
							// cria a datatable dos inscritos
							dataInscritos = FLUIGC.datatable('#dataInscritos', {
								dataRequest: [],
								search: {
									enabled: false,
								},
								renderContent: ['nome','cargo', 'empresa','email'],
								header: [
									{ title: "Nome" },
									{ title: "Cargo" },
									{ title: "Empresa" },
									{title: "E-mail"}
									
								],
								navButtons: {
									enabled: true,
								},
							}, function(err, data) {
								// DO SOMETHING (error or success)
							});
							var linhaSelecionada = myTable.selectedRows()[0];
							var linha = myTable.getRow(linhaSelecionada);
							console.log(linha)
							var c1 = DatasetFactory.createConstraint('idEvento',linha.idEvento,linha.idEvento,ConstraintType.MUST)
							var c2 = DatasetFactory.createConstraint('metadata#active', true, true, ConstraintType.MUST)
							var ds = DatasetFactory.getDataset('dsInscricoesConf', null, [c1, c2], null).values;
							for(var i = 0; i < ds.length; i++){
								var newItem ={
									nome: ds[i].nome,
									cargo: ds[i].cargo,
									empresa: ds[i].empresa,
									email: ds[i].email
								}
								dataInscritos.addRow(0, newItem)
							}

							$("#fluig-modal").on('click','[data-excel]', function(e){
								var linhaSelecionada2 = myTable.selectedRows()[0];
								var linha2 = myTable.getRow(linhaSelecionada2);
								$("#dataInscritos").table2excel({
									name: linha2.titleEvento,
									filename: linha2.titleEvento
								});
								e.stopPropagation();
							})
							$("#fluig-modal").on('click','[data-envia-email]', function(e) {
								var linhaSelecionada3 = myTable.selectedRows()[0];
								var linha3 = myTable.getRow(linhaSelecionada3);
								var destinatarios =''
								for(var i = 0; i < ds.length; i ++){
									if(i == ds.length-1){
										destinatarios += ds[i].email
									}else{
										destinatarios += ds[i].email+';'
									}
                                }
								console.log(destinatarios);
								var local = '';
								if(linha3.localEvento == "POA"){
									local = "Porto Alegre"
								}else{
									local = "Caxias do Sul"
								}
								var dsConv = DatasetFactory.getDataset('dsConvites', ["imgUrl"], [c1,c2],null).values
								var sendMail = {
									"to" : destinatarios, //emails of recipients separated by ";"
									"from" : "fluig@totvsrs.com.br", // sender
									"subject" : "Pesquisa de Satisfação do evento "+linha3.titleEvento, //   subject
									"templateId" : "templatePesquisaEventos", // Email template Id previously registered
									"dialectId"  : "pt_BR", //Email dialect , if not informed receives pt_BR , email dialect ("pt_BR", "en_US", "es")
									"param" : {
										"urlPesquisa":'http://fluighml.totvsrs.com.br:8080/portal/1/pesquisa_markting?'+linha.idEvento,
										"title": linha3.titleEvento,
										"data": linha3.dtEvento,
										"local": local,
										"imge":dsConv[0].imgUrl
									} //  Map with variables to be replaced in the template
								}
								$.ajax({
									type: "POST",
									url: "http://fluighml.totvsrs.com.br:8080/api/public/alert/customEmailSender",
                                    data : JSON.stringify(sendMail),
									contentType: "application/json",
                                }).done(function(data) {
                                    console.log(data)
                                    FLUIGC.toast({ title:'Parabéns' , message:'todos e-mails foram enviados' , type:'success' ,  });
                                })
                                .fail(function (jqXHR, textStatus) {
                                    console.log(jqXHR)
								})
								e.stopPropagation();
								
							});

							$("#fluig-modal").on('click','[data-extrair-pesquisa]',function(e) {
								e.stopPropagation();
								e.preventDefault();
								var linhaSelecionada3 = $('#div_proc tr.active');

								var item = {
									titleEvento: linhaSelecionada3.children()[0].innerText, 
									status: linhaSelecionada3.children()[1].innerText,
									dtEvento: linhaSelecionada3.children()[2].innerText,
									hrEvento: linhaSelecionada3.children()[3].innerText,
									localEvento: linhaSelecionada3.children()[4].innerText,
									idEvento: linhaSelecionada3.children()[5].innerText
								}
								contadorModal++;
								console.log("[" + e.type + "] total eh: "  + contadorModal);
								var c = DatasetFactory.createConstraint('idEvento', item.idEvento,item.idEvento, ConstraintType.MUST)
								exportCSV([c], item);
								
							})

							// Monta o 'corpo' do arquivo CSV
							function geraCSV(objArray) {
								var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
								var str = '';

								for (var i = 0; i < array.length; i++) {
									var line = '';
									for (var index in array[i]) {
										if (line != '') line += ','

										line += array[i][index];
									}

									str += line + '\r\n';
								}

								return str;
							}

							// Consulta o dataset e gera um link para download do arquivo CSV montado pela geraCSV
							function exportCSV(constraints, linha3){
								var ds = DatasetFactory.getDataset("dsPesquisa",null,constraints,null).values;
								var arr = new Array();
								arr.push({
									nome: "Nome",
									empresa: "Empresa",
									mail: "E-mail",
									fone: "Telefone",
									adequa: "Em termos de adequação (conforto higiene) o local em que o evento foi realizado",
									didatico: "Os recursos didáticos ( slides-uso do quadro-exemplos-simulações).",
									conteudo: "Relevância atualizaçao e aplicabilidade dos conteúdos realizados.",
									cargaHr: "A carga horária?",
									dominio: "Domínio (segurança e conhecimento demonstrado pelo palestrante e sua capacidade de transmitir o conteúdo).",
									pontualidade:"Pontualidade",
									ntEvento: "Nota do Evento(1 a 10)",
									visita:"Gostaria de receber uma visita da TOTVS ?",
									comentarios: "Comentários adicionais ou sugestão de pauta" 
								})
								$.each(ds, function (indexInArray, valueOfElement) { 
									arr.push({
										nome: valueOfElement.nome ,
										empresa: valueOfElement.empresa ,
										mail: valueOfElement.mail,
										fone: valueOfElement.fone ,
										adequa: valueOfElement.adequa,
										didatico: valueOfElement.didatico,
										conteudo: valueOfElement.conteudo,
										cargaHr: valueOfElement.cargaHr,
										dominio: valueOfElement.dominio,
										pontualidade: valueOfElement.pontualidade,
										ntEvento: valueOfElement.ntEvento,
										visita: valueOfElement.visita,
										comentarios: valueOfElement.comentarios 
									})
									
								});
								//console.log(arr)
								//console.log(linha3)
								var a         = document.createElement('a');
								a.href        = 'data:attachment/csv,' +  encodeURIComponent(geraCSV(arr) );
								a.target      = '_blank';
								a.innerText   ="CLIQUE PARA EXPORTAR O CSV";
								a.download    = linha3.titleEvento+"Pesquisas.csv";
								a.id = 'extraiID'
								$("#extrai").append(a);
								var el = document.getElementById('extraiID');
								el.click()
								$("#extraiID").hide();
								return true;

							}
						}
                    });
                }
            });