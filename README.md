# Layouts de eventos

Repositório para layouts de eventos

## Instalação (via repositório Git)
1. Clone o repositório do projeto
```
$ git clone https://gitlab.com/Douglas2267/mktevents.git
```

2. Copie a pasta `forms` e substitua a pasta de formulários do workspace de seu projeto no Fluig (tipicamente `C:/workspace/{SEU_PROJETO}/forms/`).
3. Copie a pasta `wcm/` para a raiz do workspace de seu projeto no Fluig (tipicamente `C:/workspace/{SEU_PROJETO}/`).
4. Copie a pasta `workflow/` para a raiz do workspace de seu projeto no Fluig (tipicamente `C:/workspace/{SEU_PROJETO}/`).
5. Exporte os formularios
6. Exporte os layouts
7. crie uma pagina com a widget url e coloque o link do freemaker do formulario `relatorioEvent/`

## Importação

1. Dentro do Eclipse, dentro da guia `Package Explorer`, clique com o botão direito escolha a opção *Importar...*
2. Selecione *Importar Processo*
3. Selecione o formato *Fluig Server*
4. Selecione o servidor Fluig e o Processo referente
5. Deixe marcada a opção *Importar formulário*
6. Clique em *Finalizar* para concluir a importação do processo


## Objetivos ##

Criar portas para o auxilo do marketing na divulgação de eventos e organização dos eventos 
assim, listando os convidados, exportação de planilhas dos convidados e planilhas sobre a 
pesquisa do evento

## Formulários ##/

### addConvites: Adiciona os convites pelo processo
- **Titulo** (*Stirng*):Titulo dos convites
- **Data** (*String*): Data de realização do evento
- **ID** (*int*): Campo identificador 
- **Horario** (*String*): horario do evento
- **Carregar imagem** (*File*): Campo usado para inserir a imagem do evento
- **Descrição** (*String*): Campo para colocar uma breve descrição sobre o evento
- **Evento Ativo** (*Radio*): Campo para verificar se o evento está ativo ou não
- **Local** (*Select*): Campo para selecionar a localidade do evento
- **URL** (*String*) [A][R]: Campo onde é gerado a url de inscrição 

### InscricoesDS: Serve como dataset de inscrições para os eventos
- **Nome** (*String*): Campo com o nome do inscrito
- **Cargo** (*String*): Campo com o cargo do inscrito
- **Empresa** (*String*): Campo com a empresa onde o inscrito trabalha
- **E-mail** (*email*): Campo com o email para contado do inscrito
- **ID** (*String*): Campo indentificador do evento que esta cadastrado

### pesquisaEventos: Serve como dataset de pesquisa de eventos
- **Nome** (*String*): Campo com o nome do inscrito
- **Telefone** (*String*): Campo com o telefone para contato do inscrito
- **Empresa** (*String*): Campo com a empresa onde o inscrito trabalha
- **E-mail** (*email*): Campo com o email para contado do inscrito
- **Adequa** (*Radio*): resposta da pesquisa
- **didatico** (*Radio*): resposta da pesquisa
- **conteudo** (*Radio*): resposta da pesquisa
- **Carga Horaria** (*Radio*): resposta da pesquisa
- **dominio** (*Radio*): resposta da pesquisa
- **Pontualidade** (*Radio*): resposta da pesquisa
- **Pontuação do evento** (*int*): pontuação geral do evento de 1 a 10
- **Cometarios** (*String*): Comentarios sobre emlhorias e sugestoes para eventos
- **ID** (*String*): Campo indentificador do evento que esta cadastrado


> Nota: **[A]** são campos preenchidos automaticamente; **[C]**, customizados; **[R]**, *readonly* e **[*]**, *required*. 


## Eventos vinculados
- Nenhuma

## Dependências e bibliotecas
- Nenhuma

