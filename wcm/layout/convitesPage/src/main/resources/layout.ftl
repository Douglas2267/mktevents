<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src='/portal/resources/js/jquery/jquery.js'></script>
        <script src='/portal/resources/js/jquery/jquery-ui.min.js'></script>
        <script src='/portal/resources/js/mustache/mustache-min.js'></script>
        <script src='/style-guide/js/fluig-style-guide.min.js'></script>

        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha1.js"></script>
        <!-- sha256 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha256.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/enc-base64.min.js"></script>
        
        <script src='/webdesk/vcXMLRPC.js'></script>
    </head>
        <title>Eventos</title>
        <style>
            .div{
                background-color: #e6e3e1;
                position: relative;
                padding: 2%;
                box-shadow: 2px 2px 2px 5px rgba(0, 0, 0, 0.2)
            }
            header{
                background-color:#242424;
                height: 10%;
            }
            
            b{
                margin-right: 1%;
            }
            .imgConvite{
                width: 100%;
                height: 30%;
                border-radius: 3%;
                margin-top: -0.6%
            }
            .btn{
                position: absolute;
                align-content: center;
                width: 50%;
                bottom: 4.5%;
            }
            .dataHora{
                vertical-align: middle;
            }
            .localEvent{
                vertical-align: middle;
            }
            #Apresentacao{
                background-image: url("http://fluighml.totvsrs.com.br:8080/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD02NzM3MSZ2ZXI9MTAwMCZmaWxlPU1vZGVsb19XYWxscGFwZXIucG5nJmNyYz0zMjIyODE0NzAxJnNpemU9MC44NjYzMzcmdUlkPTIwNTc3JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.png");
                background-size: cover;
                background-repeat: no-repeat;
                height:100%;
                width:100%;
                margin-top: -5%;
            }
            h1{
                color: white;
                position: absolute;
                bottom: 60%;
                right: 70%;
            }
            #text1{
                position: absolute;
                background: rgba(255,255,255,.8);
                clear: both;
                float: none;
                display: inline-block;
                color: #1f1e1d;
                border: 1px solid rgba(255,255,255,.3);
                border-radius: 35px;
                padding: 7px 25px;
                margin-right: 10px;
                bottom: 50%;
                right: 66%;
                width: 20%;
            }
            #text2{
                position: absolute;
                background: rgba(255,255,255,.8);
                clear: both;
                float: none;
                display: inline-block;
                color: #1f1e1d;
                border: 1px solid rgba(255,255,255,.3);
                border-radius: 35px;
                padding: 7px 25px;
                margin-right: 10px;
                bottom: 36%;
                right: 66%;
                width: 20%;
            }
            html{
                scroll-behavior: smooth
            }

            @media(min-width: 1300px){
                #text2{
                position: absolute;
                background: rgba(255,255,255,.8);
                clear: both;
                float: none;
                display: inline-block;
                color: #1f1e1d;
                border: 1px solid rgba(255,255,255,.3);
                border-radius: 35px;
                padding: 7px 25px;
                margin-right: 10px;
                bottom: 30%;
                right: 66%;
                width: 20%;
            }
            }   
            @media(max-width: 768px){
                #text1{
                    position: absolute;
                    background: rgba(255,255,255,.8);
                    clear: both;
                    float: none;
                    display: inline-block;
                    color: #1f1e1d;
                    border: 1px solid rgba(255,255,255,.3);
                    border-radius: 35px;
                    padding: 7px 25px;
                    margin-right: 10px;
                    bottom: 50%;
                    width: fit-content;
                    right: auto;
                }
                #text2{
                    position: absolute;
                    background: rgba(255,255,255,.8);
                    clear: both;
                    float: none;
                    display: inline-block;
                    color: #1f1e1d;
                    border: 1px solid rgba(255,255,255,.3);
                    border-radius: 35px;
                    padding: 7px 25px;
                    margin-right: 10px;
                    bottom: 30%;
                    width: fit-content;
                    right: auto;
                }
                h1{
                    right: auto;
                }
            }
        </style>
    </head>
    <body>
        <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#">
                <img src="https://lh3.googleusercontent.com/-1XoHxRuo0Tg/XSXbEi7BJkI/AAAAAAAAANs/rMexYSQUR8sd5_Sa2tDHzFmdztoHtpUXgCK8BGAs/s0/portal-eventos.png" alt=""/>
                    </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="#">
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#Apresentacao">Sobre</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#eventosContainer">Eventos</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                    </li>
                </ul>
                </div>
            </div>
            </nav>
        </header>
        <div id="Apresentacao">
        </div>
        <div class='fluig-style-guide'>
            <div id="eventosContainer" class="container">
                <br>
                <div id="row1" class="row">                
                </div>
            </div>
        </div>
        <br>
        <footer class="page-footer font-small blue" >
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3" style="background: lightgrey;">
                © 2019 Copyright:Totvs
            </div>
            <!-- Copyright -->
        </footer>
    </body>
    <script src="/convitesPage/resources/js/datasetfactoryauth.js"></script>
    <script src="/convitesPage/resources/js/hmac-sha1.js"></script>
    <script src="/convitesPage/resources/js/convitesPage.js"></script>
    <script src="/convitesPage/resources/js/oauth-1.0a.js"></script>
</html>