$(document).ready(function () {
    var c1 = DatasetFactoryAuth.createConstraint('valid',"sim","sim",ConstraintType.MUST)
    var c2 = DatasetFactoryAuth.createConstraint('metadata#active', true, true, ConstraintType.MUST)
    var dsConvites = DatasetFactoryAuth.getDataset('dsConvites', null, [c1,c2], ['dtEnvento;asc']).values;
    console.log(dsConvites)

    var count = 2;
    var currentRow = 'row1';

    for(var i = 0; i < dsConvites.length; i++){
        var dataI = dsConvites[i].dtEnvento.split('/')
        for(var j = 0; j < dsConvites.length; j++){
            var dataJ = dsConvites[j].dtEnvento.split('/') 
            var dtI =  new Date(parseInt(dataI[2]),parseInt(dataI[1])-1,parseInt(dataI[0]))
            var dtJ = new Date(parseInt(dataJ[2]),parseInt(dataJ[1])-1,parseInt(dataJ[0]))

            if(dtI < dtJ){
                var temp = dsConvites[i]
                dsConvites[i] = dsConvites[j]
                dsConvites[j] = temp
            }
        }
    }
    
    for(var i = 0; i <= dsConvites.length;i++){
        if((i+1)%3 == 0){
            var local = '';
            if(dsConvites[i].localEvento == "POA"){
                local = 'Porto Alegre'
            }else if(dsConvites[i].localEvento == "Serra"){
                local = 'Caxias do Sul'
            }
             // template para adicionar novo item
             $('#'+currentRow).append('<div class="col-md-4 col-sm-12 col-xs-12 col-12 boxShad" style="font-family: roboto;">'
             +'<div class="div" style="font-family: roboto; border: 1px solid white;height:370px; border-radius: .50rem;">'
                 +'<div class="imgContainer" style="background-image: url(\''+dsConvites[i].imgUrl+'\');background-size: cover;background-repeat: no-repeat;background-position: center;height:40%">'
                 +'</div>'
                 +'<br>'
                 +'<div class="titleC">'
                    +'<b>'+dsConvites[i].title+'</b>'
                 +'</div>'
                 +'<div class="dataHora">'
                     +'<i class="fluigicon fluigicon-calendar icon-sm"></i>&nbsp;'+dsConvites[i].dtEnvento+'&nbsp;<i class="fluigicon fluigicon-time icon-sm"></i>'+dsConvites[i].hrEvento
                 +'</div>'
                 +'<div class="localEvent">'
                    +'<p><i class="fluigicon fluigicon-map-marker icon-sm"></i>'+local+'</p>'
                 +'</div>'
                 +'<div class="descricao">'
                     +dsConvites[i].descricao
                 +'</div>'
                 +'<a class="btn btn-info" href="'+dsConvites[i].eventoUrl+'">Inscrever-se</a>'
             +'</div>'
         +'</div>');
            $('#eventosContainer').append('<br><div id="row'+count+'" class="row"></div>')
            currentRow = 'row'+count;
            count++;
        }else{
            var local = '';
            if(dsConvites[i].localEvento == "POA"){
                local = 'Porto Alegre'
            }else if(dsConvites[i].localEvento == "Serra"){
                local = 'Caxias do Sul'
            }
             // template para adicionar novo item
             $('#'+currentRow).append('<div class="col-md-4 col-sm-12 col-xs-12 col-12 boxShad" style="font-family: roboto;">'
             +'<div class="div" style="font-family: roboto; border: 1px solid white;height:370px; border-radius: .50rem;">'
                 +'<div class="imgContainer" style="background-image: url(\''+dsConvites[i].imgUrl+'\');background-size: cover;background-repeat: no-repeat;background-position: center;height:40%">'
                 +'</div>'
                 +'<br>'
                 +'<div class="titleC">'
                    +'<b>'+dsConvites[i].title+'</b>'
                 +'</div>'
                 +'<div class="dataHora">'
                     +'<i class="fluigicon fluigicon-calendar icon-sm"></i>&nbsp;'+dsConvites[i].dtEnvento+'&nbsp;<i class="fluigicon fluigicon-time icon-sm"></i>'+dsConvites[i].hrEvento
                 +'</div>'
                 +'<div class="localEvent">'
                    +'<p><i class="fluigicon fluigicon-map-marker icon-sm"></i>'+local+'</p>'
                 +'</div>'
                 +'<div class="descricao">'
                     +dsConvites[i].descricao
                 +'</div>'
                 +'<a class="btn btn-info" href="'+dsConvites[i].eventoUrl+'">Inscrever-se</a>'
             +'</div>'
         +'</div>');
        }
    }

    if(dsConvites.length == 0){
        $('#eventosContainer').append('<h1>Não encontrado eventos disponiveis!!</h1>')
        FLUIGC.toast({ title:'Aviso' , message:'Sem Eventos Postados!' , type:'warning' ,  });
    }
    console.log(dsConvites)
});