$(document).ready(() => {
    idEvento = getIdEvent();
    if(idEvento != undefined && idEvento != null ){
        var c = DatasetFactoryAuth.createConstraint('idEvento',idEvento,idEvento,ConstraintType.MUST);
        var c2 = DatasetFactoryAuth.createConstraint('metadata#active', true, true, ConstraintType.MUST)
        dsEvent = DatasetFactoryAuth.getDataset('dsConvites', null, [c, c2], null).values;
        if(dsEvent != undefined && dsEvent.length > 0){
            var title = dsEvent[0].title;
            var local = '';
            if(dsEvent[0].localEvento == "POA"){
                local = 'Porto Alegre'
            }else if(dsEvent[0].localEvento == "Serra"){
                local = 'Caxias do Sul'
            }
            $("#textInfo").append('<b>Inscrição para o evento '+title+'</b>');
            // template para informações sobre o evento
            $("#infEv").append('<div class="titleC">'
            +'</div>'
            +'<div class="dataHora">'
                +'<i class="fluigicon fluigicon-calendar icon-sm"></i>&nbsp;'+dsEvent[0].dtEnvento+'&nbsp;<i class="fluigicon fluigicon-time icon-sm"></i>'+dsEvent[0].hrEvento
            +'</div>'
            +'<div class="localEvent">'
                +'<p><i class="fluigicon fluigicon-map-marker icon-sm"></i>'+local+'</p>'
            +'</div>'
            +'<div class="descricao">'
                +dsEvent[0].descricao
            +'</div>')
        }else{
            FLUIGC.toast({ title:'ERRO: ' , message:"Não há evento Cadastrado neste link ou está cancelado o Evento!" , type:'danger' ,  });
        }
    }else{
        FLUIGC.toast({ title:'ERRO: ' , message:"Não há evento Cadastrado neste link ou está cancelado o Evento!" , type:'danger' ,  });
    }

    $("#conf").click(function() {
        console.log(idEvento);
        if(verificaID()){
            if($("#nome").val() != '' && $("#email").val() != '' && $("#empresa").val() != '' && $("#cargo").val() != ''){
                var c1 = DatasetFactoryAuth.createConstraint('idEvento',idEvento,idEvento,ConstraintType.MUST)
                var ds = DatasetFactoryAuth.getDataset('dsInscricoesConf', null, [c1], null).values;
                var verf = true;
                console.log(ds);
                var nome = $("#nome").val();
                var email= $("#email").val();
                for(var i = 0; i < ds.length; i++){
                    if(ds[i].email.toLowerCase() == email.toLowerCase()){
                        verf = false;
                        break;
                    }
                }

                console.log('entrei')
                var _xml;
                //Template envelope XML 
                $.ajax({
                    url: '/inscricoesEventos/resources/js/creatCard.xml',
                    async: false,
                    type: 'GET',
                    datatype: 'xml',
                    success: function (xml) {
                    _xml = $(xml)
                    },
                    fail: function (error) {
                        console.dir(error)
                    }
                });
            
                _xml.find('companyId').text('1');
                _xml.find('username').text('douglas.marques@totvs.com.br');
                _xml.find('password').text('M@caco123');
                _xml.find('colleagueId').text('4qhyhf7chbbdp9n71552412443333');
                _xml.find('parentDocumentId').text('65720');
                _xml.find("[name=nome]").text($('#nome').val());
                _xml.find('[name=cargo]').text($("#cargo").val());
                _xml.find('[name=empresa]').text($("#empresa").val());
                _xml.find('[name=email]').text($("#email").val());
                _xml.find('[name=idEvento]').text(idEvento);
            
                if(verf){
                    var request = DatasetFactoryAuth.getOAuthConfig();
                    
                    var requestData = {
                        url: request.url + "/webdesk/ECMCardService?wsdl", 
                        method:"POST",
                        content: _xml[0]                 
                    };
               
                    var auth = request.oauth.toHeader(request.oauth.authorize(requestData, request.token));
                    var header = {
                        'Access-Control-Allow-Origin'   : '*',
                        'Access-Control-Allow-Methods'  : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
                        'Access-Control-Allow-Headers'  : 'Origin, Content-Type, X-Auth-Token',
                        Authorization: auth.Authorization
                    }
                    WCMAPI.Create({
                        url: "/webdesk/ECMCardService?wsdl",
                        contentType: "text/xml",
                        dataType: "xml",
                        data: _xml[0],
                        headers:header,
                        success: function (data) {
                            console.log(data)
                            FLUIGC.toast({
                                title: 'Parabéns ',
                                message: 'Sua inscrição está confirmada',
                                type: 'success',
                                timeout: 5000
                            });
                        }
                    });

                    
                    var local = ''
                    var emailContact = ''
                    if(dsEvent[0].localEvento == "POA"){
                        local = "Porto Alegre"
                        emailContact =  "madson.belmonte@totvs.com.br"  
                    }else{
                        local = "Caxias do Sul"
                        emailContact = "karine.hermel@totvs.com.br"
                    }
                    console.log(dsEvent)
                    
                    var sendMail = {
                        "to" : email, //emails of recipients separated by ";"
                        "from" : "fluig@totvsrs.com.br", // sender
                        "subject" : dsEvent[0].title+"- você confirmou presença ", //   subject
                        "templateId" : "templateInscricoes", // Email template Id previously registered
                        "dialectId"  : "pt_BR", //Email dialect , if not informed receives pt_BR , email dialect ("pt_BR", "en_US", "es")
                        "param" : {
                            "title": dsEvent[0].title,
                            "data": dsEvent[0].dtEnvento,
                            "local": local,
                            "email": emailContact,
                            "imge":dsEvent[0].imgUrl
                        } //  Map with variables to be replaced in the template
                    }
                    var requestData2 = {
                        url: 'http://fluighml.totvsrs.com.br:8080/api/public/alert/customEmailSender', 
                        method:"POST",
                        content: JSON.stringify(sendMail)                 
                    };
               
                    var auth2 = request.oauth.toHeader(request.oauth.authorize(requestData2, request.token));
                    var header2 = {
                        'Access-Control-Allow-Origin'   : '*',
                        'Access-Control-Allow-Methods'  : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
                        'Access-Control-Allow-Headers'  : 'Origin, Content-Type, X-Auth-Token',
                        Authorization: auth2.Authorization
                    }
                    $.ajax({
                        type: "POST",
                        url: WCMAPI.getServerURL()+ "/api/public/alert/customEmailSender",
                        data : JSON.stringify(sendMail),
                        contentType: "application/json",
                        headers: header2
                    }).done(function(data) {
                        console.log(data)
                    })
                    .fail(function (jqXHR, textStatus) {
                        console.log(jqXHR)
                    })

                    setTimeout(function(){
                        window.location.replace( WCMAPI.getServerURL()+"/portal/1/pagina_de_convites")
                    }, 2500);

                }else{
                    FLUIGC.toast({ title:'Aviso:' , message:'E-mail já está cadastrado!' , type:'warning' ,  });
                }
                $('#nome').val('');
                $("#cargo").val('');
                $("#empresa").val('');
                $("#email").val('');

               
                
            }else{
                FLUIGC.toast({ title:'Aviso' , message:'Preencha todos os campos!' , type:'warning' ,  });
            }
        }else{
            FLUIGC.toast({ title:'ERRO: ' , message:"Não há evento Cadastrado neste link ou está cancelado o Evento!" , type:'danger' ,  });
            setTimeout(function(){
                window.location.replace( WCMAPI.getServerURL()+"/portal/1/pagina_de_convites")
            }, 2000);
        }
    }); 
});


function getIdEvent() {
    var arr = location.href;
        arr = arr.split('?');
        return arr[1];
}

function verificaID() {
    var c = DatasetFactoryAuth.createConstraint('idEvento',idEvento,idEvento,ConstraintType.MUST);
    var dsEvent = DatasetFactoryAuth.getDataset('dsConvites', null, [c], null).values;
    if(dsEvent != undefined && dsEvent.length > 0){
      return true;
    }else{
        return false;
    }


}

