DatasetFactoryAuth = {
    // Chamadas REST API
 getAvailableDatasets: function() {
     var APIData = {
         url: '/api/public/ecm/dataset/availableDatasets',
         method: 'GET'
     }
     
     // Faz chamada AJAX configurada
     this.restAPICall(APIData);
 },

 getDatasetStructure(datasetId) {
     var APIData = {
         url: '/api/public/ecm/dataset/datasetStructure/' + datasetId,
         method: 'GET'
     }
     
     // Faz chamada AJAX configurada
     this.restAPICall(APIData);
 },

 getDataset:function(nameDataset,fields = null, constraint = null, ordem = null){
     var cont = {
         constraints: constraint,
         fields: fields,
         name: nameDataset,
         order: ordem
     }
     var APIData = {
         url: '/api/public/ecm/dataset/datasets/',
         method: 'POST',
         content: cont
     };

     var ds = this.restAPICall(APIData);
     return ds
 },
 // Funções auxiliares padrões para chamadas REST
 getOAuthConfig: function() {
     var consumerPublic = "POST";
     var consumerSecret = "1234";
     var tokenPublic = "d54d84a3-742f-41d6-b8b6-7d98c2bba7df";
     var tokenSecret = "d7eeb2b8-c486-41a6-99cd-ca567d4bffbbb54dd474-1aae-48e4-87d9-9511b68ae815";
     var _url =  WCMAPI.getServerURL();

     // Lipa os Cookies
     removeCookies();

     var oauth = OAuth({
         consumer: {
             'public': consumerPublic, 
             'secret': consumerSecret
         },
         signature_method: 'HMAC-SHA1',
         parameter_seperator: ",",
         nonce_length: 6
     }); 

     var token = {
         'public': tokenPublic,
         'secret': tokenSecret
     };

     return {
         oauth: oauth,
         token: token,
         url: _url
     }
 },
 restAPICall: function(APICall) {
     var request = this.getOAuthConfig();
     
     var requestData = {
         url: request.url + APICall.url, 
         method: APICall.method,
         content: APICall.method == "POST" ? APICall.content : ''                  
     };

     var auth = request.oauth.toHeader(request.oauth.authorize(requestData, request.token));
     var header = {
         'Access-Control-Allow-Origin'   : '*',
         'Access-Control-Allow-Methods'  : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
         'Access-Control-Allow-Headers'  : 'Origin, Content-Type, X-Auth-Token',
         Authorization: auth.Authorization
     }
     
     var requestBody = (APICall.method == "POST") ? JSON.stringify(APICall.content) : requestData.ajaxData;
     var resp = '';
     $.ajax({
         url: requestData.url,
         type: requestData.method,
         async: false,
         data: requestBody,
         contentType: "application/json",
         headers: header
     })
     .done(function(data) {       
        resp = data.content
      
     })
     .fail(function(data) {
         console.log(data);
     });
     return resp
 },
 createConstraint:function(nomeCampo, initialValue, finalValue, type) {
     return{
         _field: nomeCampo,
         _finalValue: initialValue,
         _initialValue: finalValue,
         _type: type
     }
 }
}

function removeCookies() {
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/";
	document.cookie = "JSESSIONIDSSO=;EXPIRES=-1;" + "; path=/";
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/hiring";
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/style-guide";
}