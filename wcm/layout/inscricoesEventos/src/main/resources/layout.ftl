<html>
<head>
    <link rel='stylesheet' type='text/css' href='/style-guide/css/fluig-style-guide.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src='/portal/resources/js/jquery/jquery.js'></script>
    <script src='/portal/resources/js/jquery/jquery-ui.min.js'></script>
    <script src='/portal/resources/js/mustache/mustache-min.js'></script>
    <script src='/style-guide/js/fluig-style-guide.min.js'></script>
    <script src='/webdesk/vcXMLRPC.js'></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha1.js"></script>
        <!-- sha256 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha256.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/enc-base64.min.js"></script>
    <style>
        .required:after {
            content:" *"; 
            color: red;
        }
        #infEv{
            text-align: center
        }
        body, html {
            height: auto !important;
            background: #f4f4f4;
        }
        

    </style>
</head>
<body>
    <div class="wrap">
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
                <div class="container">
                    <a class="navbar-brand" href="http://fluighml.totvsrs.com.br:8080/portal/1/pagina_de_convites">
                    <img src="https://lh3.googleusercontent.com/-1XoHxRuo0Tg/XSXbEi7BJkI/AAAAAAAAANs/rMexYSQUR8sd5_Sa2tDHzFmdztoHtpUXgCK8BGAs/s0/portal-eventos.png" alt=""/>
                        </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                        <a class="nav-link" href="#">
                            </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                    </ul>
                    </div>
            </div>
            </nav>
        </header>
        <div class='fluig-style-guide'>
            <div style="background: white;" class="well col-md-6 col-md-offset-3">
                <div id="infEv">
                    <h3><b id="textInfo"></b></h3>
                    <p id="descricao"></p>
                    
                </div>
                <hr> 
                <div class="row">
                    <div class="form-group col-md-6 offset-md-3 col-xs-12 offset-xs-12">
                        <label for="nome" class="required">Nome Completo</label>
                        <input type="text" name="nome" id="nome" class="form-control">
                        <input type="hidden" name="idEvento" id="idEvento" class="form-control">
                    </div>
                    <div class="form-group col-md-6 offset-md-3 col-xs-12 offset-xs-12">
                        <label for="cargo" class="required">Cargo</label>
                        <input type="text" name="cargo" id="cargo" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 offset-md-3 col-xs-12 offset-xs-12">
                        <label for="empresa" class="required">Empresa</label>
                        <input type="text" name="empresa" id="empresa" class="form-control">
                    </div>
                     <div class="form-group col-md-6 offset-md-3 col-xs-12 offset-xs-12">
                        <label for="email" class="required">E-mail</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 offset-md-3 col-xs-12 offset-xs-12">
                        <button id="conf" class="form-control btn-info">Confirmar</button>
                    </div>
                </div>
            </div>
    </div>
</body>

</html>
  
    