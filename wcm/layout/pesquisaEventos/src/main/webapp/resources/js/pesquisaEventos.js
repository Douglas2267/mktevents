$(document).ready(function () {
    idEvento = getIdEvent();
    console.log(idEvento)
    if(!verificaID()){
        FLUIGC.toast({ title:'Aviso' , message:'Essa pesquisa não tem Evento Cadastrado' , type:'warning' ,  });
    }
    $(document).on('click','#enviar',function(){
        //campos da pesquisa
        var nome = $('#nome').val();
        var empresa = $('#empresa').val();
        var email = $('#mail').val();
        var fone = $('#fone').val();
        var adequa = $("input[name='adequa']:checked").val();
        var didatico = $("input[name='didatico']:checked").val();
        var conteudo = $("input[name='conteudo']:checked").val();
        var cargaHr = $("input[name='cargaHr']:checked").val();
        var dominio = $("input[name='dominio']:checked").val();
        var pontualidade = $("input[name='pontualidade']:checked").val();
        var ntEvento = $('#ntEvento').val();
        var visita = $('#visita').val();
        var comentarios = $('#comentarios').val();
        
        //if que verifica todos os campos da pesquisa não estão ''
        if(nome != '' && empresa != '' && email != '' && fone != '' && adequa != undefined && didatico != undefined && conteudo != undefined && cargaHr != undefined && dominio != undefined && pontualidade != undefined && ntEvento != '' && visita != ''){
            if(verificaID()){   
                var _xml;
                //Template envelope XML 
                $.ajax({
                    url: '/pesquisaEventos/resources/js/creatCard.xml',
                    async: false,
                    type: 'GET',
                    datatype: 'xml',
                    success: function (xml) {
                    _xml = $(xml)
                    },
                    fail: function (error) {
                        console.dir(error)
                    }
                });
                console.log(email,fone,adequa,didatico,conteudo,cargaHr,dominio,pontualidade,ntEvento,visita,comentarios)
                _xml.find('companyId').text('1');
                _xml.find('username').text('douglas.marques@totvs.com.br');
                _xml.find('password').text('M@caco123');
                _xml.find('colleagueId').text("4qhyhf7chbbdp9n71552412443333");
                _xml.find('parentDocumentId').text('66022');
                _xml.find('[name=nome]').text(nome)
                _xml.find('[name=empresa]').text(empresa)   
                _xml.find('[name=mail]').text(email)
                _xml.find('[name=idEvento]').text(idEvento)
                _xml.find('[name=fone]').text(fone)
                _xml.find('[name=adequa]').text(adequa)
                _xml.find('[name=didatico]').text(didatico)
                _xml.find('[name=conteudo]').text(conteudo)
                _xml.find('[name=cargaHr]').text(cargaHr)
                _xml.find('[name=dominio]').text(dominio)
                _xml.find('[name=pontualidade]').text(pontualidade)
                _xml.find('[name=ntEvento]').text(ntEvento)
                _xml.find('[name=visita]').text(visita)
                _xml.find('[name=comentarios]').text(comentarios) 


                var request = DatasetFactoryAuth.getOAuthConfig();
                    
                    var requestData = {
                        url: request.url + "/webdesk/ECMCardService?wsdl", 
                        method:"POST",
                        content: _xml[0]                 
                    };
               
                    var auth = request.oauth.toHeader(request.oauth.authorize(requestData, request.token));
                    var header = {
                        'Access-Control-Allow-Origin'   : '*',
                        'Access-Control-Allow-Methods'  : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
                        'Access-Control-Allow-Headers'  : 'Origin, Content-Type, X-Auth-Token',
                        Authorization: auth.Authorization
                    }
                    WCMAPI.Create({
                        url: "/webdesk/ECMCardService?wsdl",
                        contentType: "text/xml",
                        dataType: "xml",
                        data: _xml[0],
                        headers:header,
                        success: function (data) {
                            console.log(data)
                            FLUIGC.toast({
                                title: 'Obrigado: ',
                                message: 'Agradecemos por ter respondido nossa pesquisa',
                                type: 'success',
                                timeout: 5000
                            });
                        }
                    });
                // após a finalização da pesquisa, ele sera direcionado para esta pagina(Mudar em produção)
                setTimeout(function(){
                    window.location.replace( WCMAPI.getServerURL()+"/portal/1/pagina_de_convites")
                }, 2500);
            }else{
                FLUIGC.toast({ title:'Aviso' , message:'Essa pesquisa não tem Evento Cadastrado' , type:'danger' ,  });
                window.close();
            }
        }else{
            FLUIGC.toast({ title:'Aviso' , message:'Preencha todos os campos corretamente' , type:'warning' ,  });
        }
    });
  
});

function getIdEvent() {
    var arr = location.href;
        arr = arr.split('?');
        return arr[1];
}
    
function verificaID() {
    var c = DatasetFactoryAuth.createConstraint('idEvento',idEvento,idEvento,ConstraintType.MUST);
    var dsEvent = DatasetFactoryAuth.getDataset('dsConvites', null, [c], null).values;
    if(dsEvent != undefined && dsEvent.length > 0){
      return true;
    }else{
        return false;
    }
}