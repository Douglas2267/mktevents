<html lang="en">
    <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <script src='/portal/resources/js/jquery/jquery.js'></script>
            <script src='/portal/resources/js/jquery/jquery-ui.min.js'></script>
            <script src='/portal/resources/js/mustache/mustache-min.js'></script>
            <script src='/style-guide/js/fluig-style-guide.min.js'></script>
            <script src='/webdesk/vcXMLRPC.js'></script>
            <!-- cdn para mask dos inputs -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha1.js"></script>
            <!-- sha256 -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha256.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/enc-base64.min.js"></script>

            <style>
                .required:after {
                    content:" *"; 
                    color: red;
                }
                ::-webkit-scrollbar {
                    width: 0px;
                }
                body, html {
                    height: auto !important;
                    background: #f4f4f4;
                }
                .well{
                    text-align: center
                }
                #container1{
                    margin: auto
                }
                #container2{
                    margin: auto
                }
            </style>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
                <div class="container">
                    <a class="navbar-brand" href="#">
                    <img src="https://lh3.googleusercontent.com/-1XoHxRuo0Tg/XSXbEi7BJkI/AAAAAAAAANs/rMexYSQUR8sd5_Sa2tDHzFmdztoHtpUXgCK8BGAs/s0/portal-eventos.png" alt=""/>
                        </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                        <a class="nav-link" href="#">
                            </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class='fluig-style-guide'>
            <div style="background: white;" class="well col-md-6 col-md-offset-3">
                <div class="row">
                    <div id="container1">
                        <h1>Informações Pessoais</h1>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="nome" class="required">Nome</label>
                                <input type="text" name="nome" id="nome" placeholder="Ex: Leandro" class="form-control"/>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="empresa" class="required">Empresa</label>
                                <input type="text" name="empresa" id="empresa" placeholder="Ex: TOTVS..." class="form-control"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="mail" class="required">E-mail</label>
                                <input type="email" name="mail" id="mail" placeholder="EX: email@email..." class="form-control"/>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="fone" class="required">Telefone</label>
                                <input type="tel" name="fone" id="fone" placeholder="DDD.." class="form-control"/>
                            </div>
                        </div>
                    </div>				
                </div>
                <hr>
                <div class="row">
                    <div class="container2">
                        <h1>Pesquisa</h1>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="adequa" class="required">Em termos de adequação (conforto, higiene) o local em que o evento foi realizado</label>
                                <div class="radio">
                                    <label><input type="radio" name="adequa" id="adequa1" value="1">1</label>
                                    <label><input type="radio" name="adequa" id="adequa2" value="2">2</label>
                                    <label><input type="radio" name="adequa" id="adequa3" value="3">3</label>
                                    <label><input type="radio" name="adequa" id="adequa4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" >
                                <label for="" class="required">Os recursos didáticos ( slides, uso do quadro, exemplos, simulações). </label>
                                <div class="radio">
                                    <label><input type="radio" name="didatico" id="didatico1" value="1">1</label>
                                    <label><input type="radio" name="didatico" id="didatico2" value="2">2</label>
                                    <label><input type="radio" name="didatico" id="didatico3" value="3">3</label>
                                    <label><input type="radio" name="didatico" id="didatico4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="" class="required">Relevância, atualizaçao e aplicabilidade dos conteúdos realizados.</label>
                                <div class="radio">
                                    <label><input type="radio" name="conteudo" id="conteudo1" value="1">1</label>
                                    <label><input type="radio" name="conteudo" id="conteudo2" value="2">2</label>
                                    <label><input type="radio" name="conteudo" id="conteudo3" value="3">3</label>
                                    <label><input type="radio" name="conteudo" id="conteudo4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="" class="required">A carga horária </label>
                                <div class="radio">
                                    <label><input type="radio" name="cargaHr" id="cargaHr1" value="1">1</label>
                                    <label><input type="radio" name="cargaHr" id="cargaHr2" value="2">2</label>
                                    <label><input type="radio" name="cargaHr" id="cargaHr3" value="3">3</label>
                                    <label><input type="radio" name="cargaHr" id="cargaHr4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="" class="required">Domínio (segurança e conhecimento demonstrado pelo palestrante e sua capacidade de transmitir o conteúdo).</label>
                                <div class="radio">
                                    <label><input type="radio" name="dominio" id="dominio1" value="1">1</label>
                                    <label><input type="radio" name="dominio" id="dominio2" value="2">2</label>
                                    <label><input type="radio" name="dominio" id="dominio3" value="3">3</label>
                                    <label><input type="radio" name="dominio" id="dominio4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="" class="required">Pontualidade</label>
                                <div class="radio">
                                    <label><input type="radio" name="pontualidade" id="pontualidade1" value="1">1</label>
                                    <label><input type="radio" name="pontualidade" id="pontualidade2" value="2">2</label>
                                    <label><input type="radio" name="pontualidade" id="pontualidade3" value="3">3</label>
                                    <label><input type="radio" name="pontualidade" id="pontualidade4" value="4">4</label>	
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-3">
                                <label for="" class="required">Nota do Evento(1 a 10)</label>
                                <input type="number" name="ntEvento" id="ntEvento" class="form-control" min="1" max="10"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-3">
                                <label for="" class="required">Gostaria de receber uma visita da TOTVS ?</label>
                                <select name="visita" id="visita" class="form-control">
                                    <option value="sim" selected >Sim</option>
                                    <option value="nao">Não</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-3">
                                <label for="" >Comentários adicionais ou sugestão de pauta</label>
                                <textarea name="comentarios" id="comentarios" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="IdEvento" id="IdEvento" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <button id="enviar" class="btn btn-success">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#fone').mask('(00)90000-0000')
            $('#mail').mask("A", {
                translation: {
                    "A": { pattern: /[\w@\-.+]/, recursive: true }
                }
            });
        </script>
    </body>
</html>